from pathlib import Path
from gtfs_kit import Feed
from gtfs_kit import read_feed as read_gtfs
from pandas import DataFrame
from pytest import fixture
from itsim_project_creation_library import feed_processing as feedp


@fixture
def data_path():
    return Path(__file__).resolve().parent / 'data'


@fixture
def nancy_gtfs_path(data_path):
    return data_path / 'nancy.zip'


def test_extract_data_from_feed(nancy_gtfs_path):
    feed = read_gtfs(nancy_gtfs_path, dist_units='m')
    assert isinstance(feed, Feed)
    res = feedp.extract_data_from_feed(feed)
    assert isinstance(res, tuple)
    assert len(res) == 13
    (agency, stops, routes, trips, stop_times,
     calendar, calendar_dates, fare_attributes, fare_rules,
     shapes, frequencies, transfers, feed_info) = res
    for df in res:
        assert df is None or isinstance(df, DataFrame)
    columns = (
        'agency_id',
        'stop_id',
        'route_id',
        'trip_id',
        ('stop_id', 'trip_id'),
        ('service_id', 'monday', 'start_date', 'end_date'),
        ('service_id', 'date', 'exception_type'),
        ('fare_id', 'price'),
        'fare_id',
        'shape_id',
        ('trip_id', 'start_time', 'end_time'),
        ('from_stop_id', 'to_stop_id', 'transfer_type'),
        ('feed_start_date', 'feed_end_date'),
    )
    for df, column in zip(res, columns):
        if df is None:
            continue
        if isinstance(column, tuple):
            for col in column:
                assert col in df
        else:
            assert column in df
